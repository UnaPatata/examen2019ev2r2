package net.iescierva.mim.mislugares2019.datos;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import net.iescierva.mim.mislugares2019.R;
import net.iescierva.mim.mislugares2019.modelo.Lugar;

public class AdaptadorNif extends RecyclerView.Adapter<AdaptadorNif.ViewHolder> {
    protected RepositorioLugares lugares;
    protected LayoutInflater inflador;
    protected Context contexto;
    protected View.OnClickListener onClickListener;
    protected Cursor cursor;

    public AdaptadorNif(RepositorioLugares lugares, Cursor cursor) {
        this.lugares = lugares;
        this.cursor = cursor;
    }

    public Cursor getCursor() {
        return cursor;
    }

    public void setCursor(Cursor cursor) {
        this.cursor = cursor;
    }

    public Lugar lugarPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        return LugaresBD.extraeLugar(cursor);
    }

    //averigua el ROWID dada la posición en el listado (o cursor, que es lo mismo)
    public int idPosicion(int posicion) {
        cursor.moveToPosition(posicion);
        if (cursor.getCount() > 0)
            return cursor.getInt(0);
        else
            return -1;
    }

    //dada la posición en el listado averigua el ROWID
    public int posicionId(int id) {
        int pos = 0;
        while (pos < getItemCount() && idPosicion(pos) != id)
            pos++;
        return pos > getItemCount() ? -1 : pos;
    }

    // Creamos el ViewHolder con la vista de un elemento sin personalizar
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflamos la Vista desde el xml
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.elemento_nif, parent, false);
        v.setOnClickListener(onClickListener);
        return new ViewHolder(v);
    }

    // Cuando se activa el ViewHolder lo personalizamos con los datos del elemento
    @Override
    public void onBindViewHolder(ViewHolder holder, int posicion) {
        Lugar lugar = lugarPosicion(posicion);
        holder.personaliza(lugar);
        holder.itemView.setTag(new Integer(posicion));
    }

    // Número de elementos
    @Override
    public int getItemCount() {
        return cursor.getCount();
    }

    public void setOnItemClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    //Creamos nuestro ViewHolder, con los tipos de elementos a modificar
    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nombre, nif;
        public ImageView foto;

        public ViewHolder(View itemView) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombre_nif);
            nif = itemView.findViewById(R.id.nif);
            foto = itemView.findViewById(R.id.foto_nif);
        }

        // Personalizamos un ViewHolder a partir de un lugar
        public void personaliza(Lugar lugar) {

            nombre.setText(lugar.getNombre());
            int id = R.drawable.otros;
            switch (lugar.getTipo()) {
                case RESTAURANTE:
                    id = R.drawable.restaurante;
                    break;
                case BAR:
                    id = R.drawable.bar;
                    break;
                case COPAS:
                    id = R.drawable.copas;
                    break;
                case ESPECTACULO:
                    id = R.drawable.espectaculos;
                    break;
                case HOTEL:
                    id = R.drawable.hotel;
                    break;
                case COMPRAS:
                    id = R.drawable.compras;
                    break;
                case EDUCACION:
                    id = R.drawable.educacion;
                    break;
                case DEPORTE:
                    id = R.drawable.deporte;
                    break;
                case NATURALEZA:
                    id = R.drawable.naturaleza;
                    break;
                case GASOLINERA:
                    id = R.drawable.gasolinera;
                    break;
            }
            foto.setImageResource(id);
            foto.setScaleType(ImageView.ScaleType.FIT_END);
            nif.setText(lugar.getNif());
        }
    }
}
