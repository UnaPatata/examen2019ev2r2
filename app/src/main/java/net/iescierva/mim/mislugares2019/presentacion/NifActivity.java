/*
 * Copyright (c) 2020. Miguel Angel
 */

package net.iescierva.mim.mislugares2019.presentacion;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import net.iescierva.mim.mislugares2019.Aplicacion;
import net.iescierva.mim.mislugares2019.R;
import net.iescierva.mim.mislugares2019.datos.AdaptadorLugaresBD;
import net.iescierva.mim.mislugares2019.datos.AdaptadorNif;
import net.iescierva.mim.mislugares2019.datos.LugaresBD;

public class NifActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    public AdaptadorNif adaptador;
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.ItemDecoration separador;
    private LugaresBD lugares;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nif);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_nif);

        lugares = ((Aplicacion)getApplication()).getLugares();
        adaptador = new AdaptadorNif(lugares, lugares.extraeCursor());
        recyclerView.setAdapter(adaptador);

        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);


        separador = new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(separador);
    }
}
